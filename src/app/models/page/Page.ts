export interface DetailPage {
  title: string;
  description: string;
}
