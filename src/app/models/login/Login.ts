export interface Login {
  login: string;
  password: string;
}

export interface LoginResponse {
  data: TokenDetail;
  message: string;
  success: boolean;
}

export interface TokenDetail {
  token: string;
  tokenExpires: string;
}