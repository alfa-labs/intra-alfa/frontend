export interface Approver {
  message: string;
  success: boolean;
  data: ApproverItem[];
}
export interface ApproverItem {
  email: string;
}
