export interface Purchase{
  message: string;
  success: boolean;
  data: PurchaseItem[];
}

export interface PurchaseItem {
  id: number;
  status: string;
  createDate: Date;
  approveDate: Date;
  provider: string;
  totalPrice: number;
  updateDate: Date;
  requirer: string;
  initialPeriod: Date;
  finalPeriod: Date;
  autRenew: boolean;
  fiscalCodeProvider: string;
  addressProvider: string;
  mailProvider: string;
  financialRemarks: string;
  remarks: string;
  itemDescription: string;
  paymentType: string;
  providerCodeBank: string;
  providerAgBank: string;
  providerAccountBank: string;
  approver: string;
}