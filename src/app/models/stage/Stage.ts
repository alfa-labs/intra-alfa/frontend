export interface Stage {
    message: string;
    success: boolean;
    data: StageItem[];
}

export interface StageItem {
  id?: number;
  entityId: number;
  documentType: string;
  approverEmail: string[];
  status: string;
  remarks?: string;
  approveAt?: Date | null;
}
