import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import {
  ProductSwitchItem,
  ShellbarUser,
  ShellbarUserMenu,
} from '@fundamental-ngx/core';
import { ThemesService } from '@fundamental-ngx/core';
import { SideNavModel } from './models/side-navigation/side-navigation.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ThemesService],
})
export class AppComponent implements OnInit {
  globalCompact: boolean = false;
  imageUrl: any;

  cssUrl: SafeResourceUrl;
  cssCustomUrl: SafeResourceUrl;

  constructor(private router: Router) {}

  title = 'Fundamental NGX Demo';
  sideNavMain: SideNavModel[] = [];
  sideNavSecondary: SideNavModel[] = [];
  luigiOption: boolean = false;
  settings = {
    theme: 'sap_fiori_3',
    mode: 'cozy',
  };
  mobile = true;
  condensed: boolean = false;

  list: ProductSwitchItem[] = [];

  user: ShellbarUser = {
    initials: '',
  };

  userMenu: ShellbarUserMenu[] = [
    { text: 'Sign In', callback: () => this.router.navigate(['auth']) },
  ];

  ngOnInit() {
    if (screen.width >= 768) {
      this.mobile = false;
    }

    this.userMenu[1] = {
      text: 'Sign Out',
      callback: () => console.log('signout'),
    };

    this.userMenu[1] = {
      text: 'Sign Out',
      callback: () => console.log('signout'),
    };
  }

  productChangeHandle(products: ProductSwitchItem[]): void {
    this.list = products;
  }
}
