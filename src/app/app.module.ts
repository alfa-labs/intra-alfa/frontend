import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import ptBr from '@angular/common/locales/pt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApprovalModule } from './components/approval/approval.module';
import { DashboardModule } from './components/dashboard/dashboard.module';
import { LoginModule } from './components/login/login.module';
import { PurchaseOrderModule } from './components/purchase-order/purchase-order.module';
import { PipesModule } from './pipes/pipes.module';
import { SharedModule } from './shared/shared.module';

import { registerLocaleData } from '@angular/common';
import { MainGuard } from './core/guards/main-guard';
import { CoreModule } from './core/core.module';
registerLocaleData(ptBr);

@NgModule({
  declarations: [AppComponent],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    LoginModule,
    DashboardModule,
    PurchaseOrderModule,
    ApprovalModule,
    SharedModule,
    PipesModule,
    CoreModule
  ],
  providers: [
    MainGuard,
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
