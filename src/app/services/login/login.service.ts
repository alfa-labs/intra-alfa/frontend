import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from 'src/app/core/services/http/http.service';
import { Login, LoginResponse } from 'src/app/models/login/Login';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private httpService: HttpService) {}

  login(login: Login): Observable<LoginResponse> {
    return this.httpService.post('/auth/login', login);
  }
}
