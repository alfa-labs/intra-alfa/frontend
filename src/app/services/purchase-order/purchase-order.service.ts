import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from 'src/app/core/services/http/http.service';
import { Approver } from 'src/app/models/approver/Approver';
import { DetailPage } from 'src/app/models/page/Page';
import { Purchase } from 'src/app/models/purchase-order/PurchaseOrder';
import { StageItem } from 'src/app/models/stage/Stage';

@Injectable({
  providedIn: 'root',
})
export class PurchaseOrderService {
  constructor(private httpService: HttpService) {}

  getPurchasesList(): Observable<Purchase> {
    return this.httpService.get('/purchaseOrder/get-all');
  }

  getById(id: number): Observable<Purchase> {
    return this.httpService.get(`/purchaseOrder/${id}`);
  }

  createNewPurchase(purchase: any): Observable<Purchase> {
    return this.httpService.post('/purchaseOrder', purchase);
  }

  editPurchase(id: number, purchase: any): Observable<Purchase> {
    return this.httpService.put(`/purchaseOrder/${id}`, purchase);
  }

  loadApprovers(): Observable<Approver> {
    return this.httpService.get('/users/get-approvers');
  }

  setApprovers(stage: StageItem): Observable<Approver> {
    return this.httpService.post('/stage', stage);
  }

  getDetailPage(): DetailPage {
    return {
      title: 'Ordem de Compras',
      description: 'Manutenção de Ordem de Compras',
    };
  }

  getHeaderTable(): string[] {
    return [
      'Status',
      'ID',
      'Emissão',
      'Solicitante',
      'Fornecedor',
      'Valor Total',
      'Ações',
    ];
  }

  getFieldsFilter(): string[] {
    return [
      'Status',
      'ID',
      'Emissão',
      'Solicitante',
      'Fornecedor',
      'Valor Total',
    ];
  }
}
