import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageDefaultService } from './page-default-service/page-default.service';
import { PurchaseOrderService } from './purchase-order/purchase-order.service';
import { DashboardService } from './dashboard/dashboard.service';
import { ApprovalService } from './approval/approval.service';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CoreModule],
  providers: [
    PageDefaultService,
    PurchaseOrderService,
    DashboardService,
    ApprovalService,
  ],
})
export class ServicesModule {}
