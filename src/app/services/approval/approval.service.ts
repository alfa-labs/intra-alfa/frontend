import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from 'src/app/core/services/http/http.service';
import { DetailPage } from 'src/app/models/page/Page';
import { Stage, StageItem } from 'src/app/models/stage/Stage';

@Injectable({
  providedIn: 'root',
})
export class ApprovalService {
  constructor(private httpService: HttpService) {}

  getAllPurchaseByEmail(mail: string | null): Observable<Stage> {
    if (mail !== null)
      return this.httpService.get(`/stage/get-by-mail?mail=${mail}`);
    else throw 'email is empty';
  }

  approve(id: number, stage: StageItem): Observable<Stage> {
    return this.httpService.put(`/stage/approve/${id}`, stage);
  }

  reject(id: number, stage: StageItem): Observable<Stage> {
    return this.httpService.put(`/stage/reject/${id}`, stage);
  }

  getDetailPage(): DetailPage {
    return {
      title: 'Aprovação de Documentos',
      description: 'Aprovação de Documento',
    };
  }

  getHeaderTable(): string[] {
    return [
      'Status',
      'ID Aprovação',
      'Num. Documento',
      'Tipo Documento',
      'Observações',
      'Data de Aprovação',
      'Ações',
    ];
  }

  getFieldsFilter(): string[] {
    return [
      'Status',
      'ID',
      'Emissão',
      'Solicitante',
      'Fornecedor',
      'Valor Total',
    ];
  }
}
