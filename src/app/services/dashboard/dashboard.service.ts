import { Injectable } from '@angular/core';
import { DetailPage } from 'src/app/models/page/Page';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor() {}

  getDetailPage(): DetailPage {
    return {
      title: 'Dashboard',
      description: 'Intranet - Alfa Sistemas de Gestão',
    };
  }
}
