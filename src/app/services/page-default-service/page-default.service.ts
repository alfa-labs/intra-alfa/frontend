import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PageDefaultService {
  constructor() {}

  getMenu(): any {
    return {
      sideNav: [
        {
          primary: [
            {
              route: '/purchase-order',
              title: 'Ordem de Compras',
              icon: 'product',
            },
            { route: '/approval', title: 'Aprovações', icon: 'sales-document' },
          ],
        },
        {
          secondary: [{ route: '/login', title: 'Sair', icon: 'log' }],
        },
      ],
      title: 'Intra ALFA',
      contract: {
        description:
          'Manage the contracts recorded by various company representatives.',
        title: 'Contracts',
      },
      product: {
        description:
          'Manage the products recorded by various company representatives.',
        title: 'products',
      },
    };
  }
}
