import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatusToColorPipe } from './status-to-color/status-to-color.pipe';

@NgModule({
  declarations: [StatusToColorPipe],
  imports: [CommonModule],
  providers: [StatusToColorPipe],
  exports: [StatusToColorPipe],
})
export class PipesModule {}
