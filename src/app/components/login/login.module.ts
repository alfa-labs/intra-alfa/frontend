import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FixedCardLayoutModule } from '@fundamental-ngx/core/fixed-card-layout';
import { CardModule } from '@fundamental-ngx/core/card';
import { AvatarModule } from '@fundamental-ngx/core/avatar';
import { ListModule } from '@fundamental-ngx/core/list';
import { InputGroupModule } from '@fundamental-ngx/core/input-group';
import { LayoutGridModule } from '@fundamental-ngx/core/layout-grid';

import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BusyIndicatorModule, MessageToastModule } from '@fundamental-ngx/core';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FixedCardLayoutModule,
    AvatarModule,
    ListModule,
    CardModule,
    InputGroupModule,
    LayoutGridModule,
    SharedModule,
    MessageToastModule,
    BusyIndicatorModule
  ],
})
export class LoginModule {}
