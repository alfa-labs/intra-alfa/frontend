import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageToastService } from '@fundamental-ngx/core';
import { Login } from 'src/app/models/login/Login';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoading: boolean = false;
  formLogin = new UntypedFormGroup({
    login: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('', [Validators.required]),
  });

  constructor(
    private _router: Router,
    private _loginService: LoginService,
    private messageToastService: MessageToastService) {}

  ngOnInit(): void {}

  onLogin(): void {
    this.isLoading = true;
    const login: Login = this.formLogin.getRawValue();

    this._loginService.login(login).subscribe(
      (response) => {
        this.isLoading = false;
        const { data } = response;
        this._router.navigate(['dashboard']);

        sessionStorage.setItem('u-lg', login.login);
        sessionStorage.setItem('u-ssi', data.token);
      },
      (error) => {
        this.isLoading = false;
        this.showErrorMessage();
      }
    );
  }

  showErrorMessage(): void {
    const content = 'Usuário ou senha incorretos.';
    this.messageToastService.open(content, {
        duration: 3000
    });
}
}
