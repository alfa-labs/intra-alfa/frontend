import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { DialogRef, FdDate } from '@fundamental-ngx/core';

import { PurchaseItem } from 'src/app/models/purchase-order/PurchaseOrder';
import { StageItem } from 'src/app/models/stage/Stage';
import { ApprovalService } from 'src/app/services/approval/approval.service';

@Component({
  selector: 'app-approval-detail',
  templateUrl: './approval-detail.component.html',
  styleUrls: ['./approval-detail.component.scss'],
})
export class ApprovalDetailComponent implements OnInit {
  id: number;
  stage: StageItem;
  isLoading = false;
  userName = sessionStorage.getItem('u-lg');
  pending = false;
  form: UntypedFormGroup;

  constructor(
    public dialogRef: DialogRef,
    private approvalService: ApprovalService
  ) {}

  ngOnInit() {
    this.loadForm(this.dialogRef.data.purchase);
    this.id = this.dialogRef.data.stage.id;
    this.pending = this.dialogRef.data.purchase.status === 'P';
    this.stage = this.dialogRef.data.stage;
  }

  submitForm(): void {
    this.isLoading = true;
  }

  loadForm(purchase: PurchaseItem): void {
    this.form = new UntypedFormGroup({
      createDate: new UntypedFormControl({
        value: FdDate.getFdDateByDate(new Date(purchase.createDate)),
        disabled: true,
      }),
      requirer: new UntypedFormControl({ value: purchase.requirer, disabled: true }),
      initialPeriod: new UntypedFormControl({
        value: FdDate.getFdDateByDate(new Date(purchase.initialPeriod)),
        disabled: true,
      }),
      finalPeriod: new UntypedFormControl({
        value: FdDate.getFdDateByDate(new Date(purchase.finalPeriod)),
        disabled: true,
      }),
      autRenew: new UntypedFormControl({
        value: purchase.autRenew,
        disabled: true,
      }),
      fiscalCodeProvider: new UntypedFormControl({
        value: purchase.fiscalCodeProvider,
        disabled: true,
      }),
      addressProvider: new UntypedFormControl({
        value: purchase.addressProvider,
        disabled: true,
      }),
      mailProvider: new UntypedFormControl({
        value: purchase.mailProvider,
        disabled: true,
      }),
      financialRemarks: new UntypedFormControl({
        value: purchase.financialRemarks,
        disabled: true,
      }),
      provider: new UntypedFormControl({
        value: purchase.provider,
        disabled: true,
      }),
      remarks: new UntypedFormControl({
        value: purchase.remarks,
        disabled: true,
      }),
      paymentType: new UntypedFormControl({
        value: purchase.paymentType,
        disabled: true,
      }),
      providerCodeBank: new UntypedFormControl({
        value: purchase.providerCodeBank,
        disabled: true,
      }),
      providerAgBank: new UntypedFormControl({
        value: purchase.providerAgBank,
        disabled: true,
      }),
      providerAccountBank: new UntypedFormControl({
        value: purchase.providerAccountBank,
        disabled: true,
      }),
      totalPrice: new UntypedFormControl({
        value: purchase.totalPrice,
        disabled: true,
      }),
      status: new UntypedFormControl({
        value: purchase.status,
        disabled: true,
      }),
      approver: new UntypedFormControl({
        value: purchase.approver,
        disabled: true,
      }),
    });
  }

  onApprove(): void {
    this.isLoading = true;
    this.stage['status'] = 'A';

    this.approvalService.approve(this.id, this.stage).subscribe(
      (data) => {
        this.isLoading = false;
        this.dialogRef.close();
      },
      (error) => {
        this.isLoading = false;
        console.log('error', error);
      }
    );
  }

  onReject(): void {
    this.isLoading = true;
    this.stage['status'] = 'R';

    this.approvalService.reject(this.id, this.stage).subscribe(
      (data) => {
        this.isLoading = false;
        this.dialogRef.close();
      },
      (error) => {
        this.isLoading = false;
        console.log('error', error);
      }
    );
  }
}
