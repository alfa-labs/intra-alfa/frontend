import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  TableModule,
  MultiInputModule,
  BusyIndicatorModule,
  MenuModule,
  LayoutPanelModule,
  PaginationModule,
  ButtonModule,
  InfoLabelModule,
  BreadcrumbModule,
  DialogModule,
  FormModule,
  LayoutGridModule,
  PanelModule,
  DatePickerModule,
  FdDatetimeModule,
  TabsModule,
} from '@fundamental-ngx/core';

import { ApprovalComponent } from './approval.component';
import { ApprovalRoutingModule } from './approval-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ApprovalDetailComponent } from './approval-detail/approval-detail.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ApprovalComponent, ApprovalDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ApprovalRoutingModule,
    BusyIndicatorModule,
    MultiInputModule,
    TableModule,
    ButtonModule,
    InfoLabelModule,
    MultiInputModule,
    BreadcrumbModule,
    MenuModule,
    LayoutPanelModule,
    PaginationModule,
    PipesModule,
    DialogModule,
    PanelModule,
    FormModule,
    ReactiveFormsModule,
    LayoutGridModule,
    LayoutPanelModule,
    DatePickerModule,
    FdDatetimeModule,
    TabsModule
  ],
})
export class ApprovalModule {}
