import { Component, OnInit } from '@angular/core';
import { DialogService } from '@fundamental-ngx/core';

import { DetailPage } from 'src/app/models/page/Page';
import { StageItem } from 'src/app/models/stage/Stage';
import { ApprovalService } from 'src/app/services/approval/approval.service';
import { PurchaseOrderService } from 'src/app/services/purchase-order/purchase-order.service';
import { ApprovalDetailComponent } from './approval-detail/approval-detail.component';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.scss'],
})
export class ApprovalComponent implements OnInit {
  globalCompact: boolean;
  purchases: StageItem[];
  multiInputPurchases: string[];
  tableHeaders: string[];
  detailPage: DetailPage;
  contract: any = null;
  totalContracts: number = 10;
  searching = false;
  currentPage = 1;
  loading = false;
  mobile: boolean = true;

  constructor(
    private approvalService: ApprovalService,
    private purchaseOrderService: PurchaseOrderService,
    private dialogService: DialogService
  ) {

    this.multiInputPurchases = this.purchaseOrderService.getFieldsFilter();
    this.tableHeaders = this.approvalService.getHeaderTable();
    this.detailPage = this.approvalService.getDetailPage();

  }

  ngOnInit(): void {
    this.loadItems();
  }

  private loadItems(): void {
    const lg = sessionStorage.getItem('u-lg');

    this.approvalService.getAllPurchaseByEmail(lg).subscribe(
      (respose) => {
        const { data } = respose;
        this.purchases = data;
      },
      (error) => {
        console.log('error', error);
      }
    );
  }

  onShowDetail(stage: StageItem): void {
    this.purchaseOrderService.getById(stage.entityId).subscribe(
      (response) => {
        const { data } = response;

        this.dialogService
          .open(ApprovalDetailComponent, {
            data: {
              purchase: data,
              stage: stage,
            },
            responsivePadding: true,
          })
          .afterClosed.subscribe(
            (result) => {
              window.location.reload();
            },
            () => {}
          );
      },
      (error) => {
        console.log('error', error);
      }
    );
  }
}
