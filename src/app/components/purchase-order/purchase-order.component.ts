import { Component, OnInit, ViewChild } from '@angular/core';
import { CdkTable } from '@angular/cdk/table';
import { Subscription } from 'rxjs';

import { PurchaseOrderService } from 'src/app/services/purchase-order/purchase-order.service';
import { DetailPage } from 'src/app/models/page/Page';
import {
  Purchase,
  PurchaseItem,
} from 'src/app/models/purchase-order/PurchaseOrder';
import { DialogService, NotificationService } from '@fundamental-ngx/core';
import { PurchaseOrderDetailComponent } from './purchase-order-detail/purchase-order-detail.component';
import { PurchaseOrderSelectApproverComponent } from './purchase-order-select-approver/purchase-order-select-approver.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss'],
})
export class PurchaseOrderComponent implements OnInit {
  globalCompact: boolean;
  purchases: PurchaseItem[];
  selected: Purchase[] = [];
  subscription: Subscription;
  multiInputPurchases: string[];
  tableHeaders: string[];
  detailPage: DetailPage;
  contract: any = null;
  totalContracts: number = 10;
  searching = false;
  currentPage = 1;
  loading = false;
  mobile: boolean = true;

  @ViewChild('table', { static: false }) table: CdkTable<{}[]>;

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private router: Router
  ) {
    this.multiInputPurchases = this.purchaseOrderService.getFieldsFilter();
    this.tableHeaders = this.purchaseOrderService.getHeaderTable();
    this.detailPage = this.purchaseOrderService.getDetailPage();
  }

  ngOnInit(): void {
    if (screen.width >= 1024) {
      this.mobile = false;
    }

    this.loadItems();
  }

  loadItems() {
    this.purchaseOrderService.getPurchasesList().subscribe(
      (items) => {
        const { data } = items;
        this.purchases = data;
      },
      (error) => {
        //this.notificationService.open('Erro ao carregar items', 'atenção');
      }
    );
  }

  onNewPurchaseOrder(): void {
    this.dialogService
      .open(PurchaseOrderDetailComponent, {
        data: {
          isEdit: false,
          isShow: false,
          isCreate: true,
        },
        responsivePadding: true,
      })
      .afterClosed.subscribe(
        (result) => {
          window.location.reload();
        },
        () => {}
      );
  }

  onDeletePurchaseOrder(): void {}

  onEditPurchaseOrder(purchase: PurchaseItem): void {
    this.dialogService
      .open(PurchaseOrderDetailComponent, {
        data: {
          isEdit: true,
          isShow: false,
          isCreate: false,
          purchase,
        },
        responsivePadding: true,
      })
      .afterClosed.subscribe(
        (result) => {
          window.location.reload();
        },
        () => {}
      );
  }

  onShowPurchaseOrder(purchase: PurchaseItem): void {
    this.dialogService
      .open(PurchaseOrderDetailComponent, {
        data: {
          isEdit: false,
          isShow: true,
          isCreate: false,
          purchase,
        },
        responsivePadding: true,
      })
      .afterClosed.subscribe(
        (result) => {
          window.location.reload();
        },
        () => {}
      );
  }

  onSendToApprove(purchase: PurchaseItem): void {
    this.dialogService
      .open(PurchaseOrderSelectApproverComponent, {
        responsivePadding: true,
        data: {
          id: purchase.id,
          documentType: 'OC',
        },
      })
      .afterClosed.subscribe(
        (result) => {
            window.location.reload();
        },
        () => {}
      );
  }
}
