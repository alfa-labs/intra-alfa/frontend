import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  TableModule,
  MultiInputModule,
  BusyIndicatorModule,
  MenuModule,
  LayoutPanelModule,
  PaginationModule,
  ButtonModule,
  InfoLabelModule,
  BreadcrumbModule,
  DialogModule,
  FormModule,
  LayoutGridModule,
  DatePickerModule,
  FdDatetimeModule,
  InputGroupModule,
} from '@fundamental-ngx/core';

import { PanelModule } from '@fundamental-ngx/core/panel';
import { NgxMaskModule, IConfig } from 'ngx-mask';

import { PurchaseOrderComponent } from './purchase-order.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { PurchaseOrderDetailComponent } from './purchase-order-detail/purchase-order-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PurchaseOrderSelectApproverComponent } from './purchase-order-select-approver/purchase-order-select-approver.component';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    PurchaseOrderComponent,
    PurchaseOrderDetailComponent,
    PurchaseOrderSelectApproverComponent,
  ],
  imports: [
    NgxMaskModule.forRoot(maskConfig),
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    SharedModule,
    PaginationModule,
    LayoutPanelModule,
    MenuModule,
    PipesModule,
    BusyIndicatorModule,
    MultiInputModule,
    TableModule,
    ButtonModule,
    InfoLabelModule,
    BreadcrumbModule,
    DialogModule,
    FormModule,
    LayoutGridModule,
    DatePickerModule,
    FdDatetimeModule,
    PanelModule,
    InputGroupModule,
  ],
})
export class PurchaseOrderModule {}
