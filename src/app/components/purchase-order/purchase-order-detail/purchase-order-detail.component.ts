import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogRef, DialogService, FdDate } from '@fundamental-ngx/core';
import { BehaviorSubject } from 'rxjs';
import { PurchaseItem } from 'src/app/models/purchase-order/PurchaseOrder';

import { PurchaseOrderService } from 'src/app/services/purchase-order/purchase-order.service';

@Component({
  selector: 'app-purchase-order-detail',
  templateUrl: './purchase-order-detail.component.html',
  styleUrls: ['./purchase-order-detail.component.scss'],
})
export class PurchaseOrderDetailComponent implements OnInit {
  id: number;
  isLoading = false;
  userName = sessionStorage.getItem('u-lg');
  editMode = false;
  showMode = false;
  createMode = false;
  globalCompact: any = false;
  form: UntypedFormGroup;

  constructor(
    public dialogRef: DialogRef,
    private dialogService: DialogService,
    private purchaseOrderService: PurchaseOrderService,
    private router: Router
  ) {
    this.globalCompact = new BehaviorSubject<boolean>(false);
  }

  ngOnInit() {
    this.editMode = this.dialogRef.data.isEdit;
    this.showMode = this.dialogRef.data.isShow;
    this.createMode = this.dialogRef.data.isCreate;

    if (this.editMode || this.showMode) {
      this.loadForm(this.dialogRef.data.purchase);
      this.id = this.dialogRef.data.purchase.id;
    }

    if (this.createMode) {
      this.initForm();
    }
  }

  initForm() {
    this.form = new UntypedFormGroup({
      createDate: new UntypedFormControl({ value: new FdDate(), disabled: true }, [
        Validators.required,
      ]),
      requirer: new UntypedFormControl({ value: this.userName, disabled: true }, [
        Validators.required,
      ]),
      initialPeriod: new UntypedFormControl('', [Validators.required]),
      finalPeriod: new UntypedFormControl('', [Validators.required]),
      autRenew: new UntypedFormControl(false, [Validators.required]),
      fiscalCodeProvider: new UntypedFormControl('', []),
      addressProvider: new UntypedFormControl('', [Validators.required]),
      mailProvider: new UntypedFormControl('', [
        Validators.required,
        Validators.email,
      ]),
      financialRemarks: new UntypedFormControl('', [Validators.required]),
      provider: new UntypedFormControl('', [Validators.required]),
      remarks: new UntypedFormControl('', [Validators.required]),
      paymentType: new UntypedFormControl('', [Validators.required]),
      providerCodeBank: new UntypedFormControl('', []),
      providerAgBank: new UntypedFormControl('', []),
      providerAccountBank: new UntypedFormControl('', []),
      totalPrice: new UntypedFormControl('', [Validators.required]),
      status: new UntypedFormControl('', []),
      approver: new UntypedFormControl('', []),
      itemDescription: new UntypedFormControl('', [Validators.required]),
    });
  }

  submitForm(): void {
    this.isLoading = true;
    const payload = this.form.getRawValue();

    payload['createDate'] = new Date(
      payload['createDate'].getTimeStamp()
    ).toISOString();
    payload['initialPeriod'] = new Date(
      payload['initialPeriod'].getTimeStamp()
    ).toISOString();
    payload['finalPeriod'] = new Date(
      payload['finalPeriod'].getTimeStamp()
    ).toISOString();

    if (this.createMode) {
      this.purchaseOrderService.createNewPurchase(payload).subscribe(
        (data) => {
          this.isLoading = false;
          this.router.navigate(['purchase-order']);
          this.dialogRef.close();

          window.location.reload();
        },
        (error) => {
          this.isLoading = false;
          console.log('error');
        }
      );
    } else if (this.editMode) {
      payload['id'] = this.id;
      this.purchaseOrderService.editPurchase(this.id, payload).subscribe(
        (data) => {
          this.isLoading = false;
          this.router.navigate(['purchase-order']);
          this.dialogRef.close();

          window.location.reload();
        },
        (error) => {
          this.isLoading = false;
          console.log('error');
        }
      );
    }
  }

  loadForm(purchase: PurchaseItem): void {
    this.form = new UntypedFormGroup({
      createDate: new UntypedFormControl(
        {
          value: FdDate.getFdDateByDate(new Date(purchase.createDate)),
          disabled: true,
        },
        [Validators.required]
      ),
      requirer: new UntypedFormControl({ value: purchase.requirer, disabled: true }, [
        Validators.required,
      ]),
      initialPeriod: new UntypedFormControl(
        {
          value: FdDate.getFdDateByDate(new Date(purchase.initialPeriod)),
          disabled: this.showMode,
        },
        [Validators.required]
      ),
      finalPeriod: new UntypedFormControl({
        value: FdDate.getFdDateByDate(new Date(purchase.finalPeriod)),
        disabled: this.showMode,
      }),
      autRenew: new UntypedFormControl(
        { value: purchase.autRenew, disabled: this.showMode },
        [Validators.required]
      ),
      fiscalCodeProvider: new UntypedFormControl(
        {
          value: purchase.fiscalCodeProvider,
          disabled: this.showMode,
        },
      ),
      addressProvider: new UntypedFormControl(
        {
          value: purchase.addressProvider,
          disabled: this.showMode,
        },
        [Validators.required]
      ),
      mailProvider: new UntypedFormControl(
        {
          value: purchase.mailProvider,
          disabled: this.showMode,
        },
        [Validators.required, Validators.email]
      ),
      financialRemarks: new UntypedFormControl(
        {
          value: purchase.financialRemarks,
          disabled: this.showMode,
        },
        [Validators.required]
      ),
      provider: new UntypedFormControl(
        { value: purchase.provider, disabled: this.showMode },
        [Validators.required]
      ),
      remarks: new UntypedFormControl(
        { value: purchase.remarks, disabled: this.showMode },
        [Validators.required]
      ),
      paymentType: new UntypedFormControl(
        {
          value: purchase.paymentType,
          disabled: this.showMode,
        },
        [Validators.required]
      ),
      providerCodeBank: new UntypedFormControl(
        {
          value: purchase.providerCodeBank,
          disabled: this.showMode,
        },
        []
      ),
      providerAgBank: new UntypedFormControl(
        {
          value: purchase.providerAgBank,
          disabled: this.showMode,
        },
        []
      ),
      providerAccountBank: new UntypedFormControl(
        {
          value: purchase.providerAccountBank,
          disabled: this.showMode,
        },
        []
      ),
      totalPrice: new UntypedFormControl(
        {
          value: purchase.totalPrice,
          disabled: this.showMode,
        },
        [Validators.required]
      ),
      status: new UntypedFormControl(
        { value: purchase.status, disabled: this.showMode },
        []
      ),
      approver: new UntypedFormControl(
        { value: purchase.approver, disabled: this.showMode },
        []
      ),
      itemDescription: new UntypedFormControl(
        { value: purchase.itemDescription, disabled: this.showMode },
        []
      ),
    });
  }
}
