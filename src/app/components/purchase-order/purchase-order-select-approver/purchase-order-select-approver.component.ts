import { Component, OnInit } from '@angular/core';
import { DialogRef, DialogService } from '@fundamental-ngx/core';
import { ApproverItem } from 'src/app/models/approver/Approver';
import { StageItem } from 'src/app/models/stage/Stage';
import { PurchaseOrderService } from 'src/app/services/purchase-order/purchase-order.service';

@Component({
  selector: 'app-purchase-order-select-approver',
  templateUrl: './purchase-order-select-approver.component.html',
  styleUrls: ['./purchase-order-select-approver.component.scss'],
})
export class PurchaseOrderSelectApproverComponent implements OnInit {
  isLoading = false;
  approvers: [];
  approverList: ApproverItem[];
  objectValuesDisplayFn = (v: any): string => v.email;
  objectValuesValueFn = (v: any): string => v.email;

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private dialogService: DialogService,
    public dialogRef: DialogRef
  ) {}

  ngOnInit(): void {
    this.loadApproverList();
  }

  onCloseModal(): void {}

  onConfirmModal(): void {
    this.isLoading = true;

    const stage: StageItem = {
      entityId: this.dialogRef.data.id,
      approverEmail: this.approvers,
      documentType: this.dialogRef.data.documentType,
      status: 'P',
      remarks: 'Aguardando Aprovação',
      approveAt: null
    };

    this.purchaseOrderService.setApprovers(stage).subscribe(
      (data) => {
        this.isLoading = false;
        this.dialogRef.close();
      },
      (error) => {
        this.isLoading = false;
        console.log('error', error);
      }
    );
  }

  private loadApproverList(): void {
    this.purchaseOrderService.loadApprovers().subscribe(
      (response) => {
        const { data } = response;
        this.approverList = data;
      },
      (error) => {}
    );
  }
}
