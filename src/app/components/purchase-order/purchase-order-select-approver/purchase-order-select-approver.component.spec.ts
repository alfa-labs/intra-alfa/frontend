import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderSelectApproverComponent } from './purchase-order-select-approver.component';

describe('PurchaseOrderSelectApproverComponent', () => {
  let component: PurchaseOrderSelectApproverComponent;
  let fixture: ComponentFixture<PurchaseOrderSelectApproverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseOrderSelectApproverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderSelectApproverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
