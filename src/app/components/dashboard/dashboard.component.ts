import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  dashboard: any;
  mobile: boolean = true;

  constructor(private dashboardService: DashboardService) {
    this.dashboard = this.dashboardService.getDetailPage();
  }

  ngOnInit() {
    if (screen.width >= 1024) {
      this.mobile = false;
    }
  }
}
