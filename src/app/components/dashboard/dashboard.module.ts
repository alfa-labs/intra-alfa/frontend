import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import {
  LayoutGridModule,
  BusyIndicatorModule,
  FixedCardLayoutModule,
  AvatarModule,
  ListModule,
  CardModule,
  InputGroupModule,
  TableModule,
  LayoutPanelModule,
  InfoLabelModule,
  BreadcrumbModule,
} from '@fundamental-ngx/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FixedCardLayoutModule,
    AvatarModule,
    ListModule,
    CardModule,
    InputGroupModule,
    LayoutGridModule,
    BusyIndicatorModule,
    TableModule,
    LayoutPanelModule,
    InfoLabelModule,
    SharedModule,
    PipesModule,
    BreadcrumbModule,
  ],
})
export class DashboardModule {}
