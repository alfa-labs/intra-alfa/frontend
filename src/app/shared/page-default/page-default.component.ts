import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import {
  ProductSwitchItem,
  ShellbarUser,
  ShellbarUserMenu,
} from '@fundamental-ngx/core';
import { Subscription } from 'rxjs';
import { SideNavModel } from 'src/app/models/side-navigation/side-navigation.model';

import { PageDefaultService } from 'src/app/services/page-default-service/page-default.service';

@Component({
  selector: 'app-page-default',
  templateUrl: './page-default.component.html',
  styleUrls: ['./page-default.component.scss'],
})
export class PageDefaultComponent implements OnInit {
  globalCompact: boolean = false;
  imageUrl: any;
  cssUrl: SafeResourceUrl;
  cssCustomUrl: SafeResourceUrl;

  constructor(
    private router: Router,
    private pageDefaultService: PageDefaultService
  ) {}

  title = 'Fundamental NGX Demo';
  sideNavMain: SideNavModel[] = [];
  sideNavSecondary: SideNavModel[] = [];
  mainInfo: any;
  luigiOption: boolean = false;
  settings = {
    theme: 'sap_fiori_3',
    mode: 'cozy',
  };
  mobile = true;
  condensed: boolean = false;

  user: ShellbarUser = {
    initials: '',
  };

  userMenu: ShellbarUserMenu[] = [
    { text: 'Sign In', callback: () => this.router.navigate(['auth']) },
  ];

  ngOnInit() {
    this.mainInfo = this.pageDefaultService.getMenu();
    if (screen.width >= 768) {
      this.mobile = false;
    }

    this.title = this.mainInfo.title;
    this.sideNavMain = this.mainInfo.sideNav[0].primary;
    this.sideNavSecondary = this.mainInfo.sideNav[1].secondary;

    this.userMenu[1] = {
      text: 'Sign In',
      callback: () => this.router.navigate(['auth']),
    };

    this.user = {
      initials:
        'account.first'.charAt(0).toLocaleLowerCase() +
        'account.last'.charAt(0).toLocaleLowerCase(),
    };
  }
}
