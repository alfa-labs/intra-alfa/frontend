import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  AlertModule,
  AvatarModule,
  BusyIndicatorModule,
  ButtonModule,
  DatePickerModule,
  DialogModule,
  FormModule,
  IconModule,
  InfoLabelModule,
  InlineHelpModule,
  LayoutGridModule,
  LayoutPanelModule,
  ListModule,
  MultiInputModule,
  NestedListModule,
  NotificationModule,
  PaginationModule,
  ProductSwitchModule,
  PopoverModule,
  SelectModule,
  ShellbarModule,
  SideNavigationModule,
  TableModule,
  CheckboxModule,
  MenuModule,
} from '@fundamental-ngx/core';

import { PageDefaultComponent } from './page-default.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [PageDefaultComponent],
  imports: [
    CommonModule,
    AlertModule,
    AvatarModule,
    BusyIndicatorModule,
    ButtonModule,
    DatePickerModule,
    DialogModule,
    FormModule,
    IconModule,
    InfoLabelModule,
    InlineHelpModule,
    LayoutGridModule,
    LayoutPanelModule,
    ListModule,
    MultiInputModule,
    NestedListModule,
    NotificationModule,
    PaginationModule,
    ProductSwitchModule,
    PopoverModule,
    SelectModule,
    ShellbarModule,
    SideNavigationModule,
    TableModule,
    CheckboxModule,
    MenuModule,
    RouterModule,
  ],
  exports: [PageDefaultComponent],
})
export class PageDefaultModule {}
