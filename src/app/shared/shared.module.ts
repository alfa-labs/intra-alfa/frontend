import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageDefaultModule } from './page-default/page-default.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, PageDefaultModule],
  exports: [PageDefaultModule],
})
export class SharedModule {}
