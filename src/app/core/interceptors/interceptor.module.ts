import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { RequestInteceptor } from './request-interceptor';
import { ResponseInteceptor } from './response-interceptor';

@NgModule({
  providers: [
    RequestInteceptor,
    ResponseInteceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInteceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInteceptor,
      multi: true
    }
  ],
})
export class InterceptorModule {}
