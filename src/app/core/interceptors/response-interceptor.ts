import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const ENDPOINT_LOGIN = '/api/v1/authentication/login';

@Injectable({
  providedIn: 'root'
})
export class ResponseInteceptor implements HttpInterceptor {
  constructor(private router: Router){}

  intercept(
    original_request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(original_request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          const { status, url } = event;

          if(status === 401) {
            this.router.navigate(['login']);
          }
        }
        return event;
      }));
  }
}
