import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

const BASE_URL = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class RequestInteceptor implements HttpInterceptor {
  intercept(
    original_request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const requestWithDomain = this.appendDomainToUrl(original_request);
    const sessionId = sessionStorage.getItem('u-ssi');
    const httpRequest = requestWithDomain.clone({
      headers: requestWithDomain.headers
        .set('Authorization', `Bearer ${sessionId}`)
    });

    return next.handle(httpRequest);
  }

  private appendDomainToUrl(request: HttpRequest<any>): HttpRequest<any> {
    const domain = BASE_URL;

    const urlWithDomain = `${domain}${request.url}`;
    return request.clone({
      url: urlWithDomain
    });
  }
}

