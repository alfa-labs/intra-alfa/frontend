import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpService } from './services/http/http.service';
import { InterceptorModule } from './interceptors/interceptor.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [HttpClientModule, ReactiveFormsModule, InterceptorModule],
  providers: [HttpService],
})
export class CoreModule {}
