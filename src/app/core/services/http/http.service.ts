import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  public get(endpoint: string, queryParams = {}): Observable<any> {
    const params = new HttpParams({
      fromObject: queryParams,
    });
    return this.http.get(endpoint, { params }).pipe(take(1));
  }

  public getBlob(endpoint: string): Observable<any> {
    return this.http.get(endpoint, { responseType: 'blob' }).pipe(take(1));
  }

  public post(
    endpoint: string,
    payload: object = {},
    queryParams = {}
  ): Observable<any> {
    const params = new HttpParams({
      fromObject: queryParams,
    });
    return this.http.post(endpoint, payload, { params: params }).pipe(take(1));
  }

  public put(endpoint: string, payload: object): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Access-Control-Allow-Origin', '*');

    return this.http.put(endpoint, payload, { headers }).pipe(take(1));
  }

  public delete(endpoint: string): Observable<any> {
    return this.http.delete(endpoint).pipe(take(1));
  }

  public getAsync(endpoint: string, queryParams = {}): Promise<any> {
    const params = new HttpParams({
      fromObject: queryParams,
    });
    return this.http.get(endpoint, { params }).toPromise();
  }
}
