#!/bin/sh

set -ex

npm install
ng build

BUILD_PATH=dist/intra-alfa/
BUCKET=s3://alfa-web/

aws s3 cp $BUILD_PATH $BUCKET --recursive --exclude "*.js" --exclude "*.css" --acl public-read --profile personal-aws
aws s3 cp $BUILD_PATH $BUCKET --recursive --exclude "*" --include "*.js" --content-type "text/javascript" --acl public-read --profile personal-aws
aws s3 cp $BUILD_PATH $BUCKET --recursive --exclude "*" --include "*.css" --content-type "text/css" --acl public-read --profile personal-aws

aws cloudfront create-invalidation --distribution-id EMPHXGHDXUTDM --paths "/*" --profile personal-aws